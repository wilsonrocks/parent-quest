const xpReducer = (totalSoFar, quest) => totalSoFar + quest.xp;

module.exports = {
  eleventyComputed: {
    totalAvailable: (data) => {
      const quests = Object.values(data.quests).flat();
      return quests.reduce(xpReducer, 0);
    },
    totalLow: (data) => {
      const quests = Object.values(data.quests).flat();
      return quests.reduce(xpReducer, 0) / 3;
    },
    totalHigh: (data) => {
      const quests = Object.values(data.quests).flat();
      return (quests.reduce(xpReducer, 0) * 2) / 3;
    },

    sewersAvailable: (data) => {
      const { sewers } = data.quests;
      return sewers.reduce(xpReducer, 0);
    },
    sewersLow: (data) => {
      const { sewers } = data.quests;
      return sewers.reduce(xpReducer, 0) / 3;
    },
    sewersHigh: (data) => {
      const { sewers } = data.quests;
      return (sewers.reduce(xpReducer, 0) * 2) / 3;
    },

    courtAvailable: (data) => {
      const { court } = data.quests;
      return court.reduce(xpReducer, 0);
    },
    courtLow: (data) => {
      const { court } = data.quests;
      return court.reduce(xpReducer, 0) / 3;
    },
    courtHigh: (data) => {
      const { court } = data.quests;
      return (court.reduce(xpReducer, 0) * 2) / 3;
    },

    commonAvailable: (data) => {
      const { common } = data.quests;
      return common.reduce(xpReducer, 0);
    },
    commonLow: (data) => {
      const { common } = data.quests;
      return common.reduce(xpReducer, 0) / 3;
    },
    commonHigh: (data) => {
      const { common } = data.quests;
      return (common.reduce(xpReducer, 0) * 2) / 3;
    },

    cemeteryAvailable: (data) => {
      const { cemetery } = data.quests;
      return cemetery.reduce(xpReducer, 0);
    },
    cemeteryLow: (data) => {
      const { cemetery } = data.quests;
      return cemetery.reduce(xpReducer, 0) / 3;
    },
    cemeteryHigh: (data) => {
      const { cemetery } = data.quests;
      return (cemetery.reduce(xpReducer, 0) * 2) / 3;
    },
  },
};
