# Parent Quest

## What is it?

A console style RPG about having a baby made as a gift for my friend made in Vanilla JS. (with 11ty to build)

Players click to compete 'quests' and when they get to a new level, It notifies me (via an IFTT webhook generating an email) and I post them some gifts.

### Try it

[Try it](https://parent-quest.netlify.app/) - sadly it currently only works in Google Chrome due to heavy use of the `<dialog>` element.

```bash
npx eleventy serve # starts a dev server
```

### What did I learn

- 11ty is a good clean way of applying layouts to a page
- You can do a lot with vanilla JS, but past a certain level of complexity it's hard to keep it clean. I wished I'd used React and xState by the end of it.
- The CSS animation with the baby flying around and the moving backgrounds were fun :D

### What could you do with it

This could be developed into a bespoke gift for geeks who are to become parents.
