module.exports = function (eleventyConfig) {
  // Output directory: _site
  eleventyConfig.addPassthroughCopy('assets');
  eleventyConfig.addPassthroughCopy('styles.css');
  eleventyConfig.addPassthroughCopy('game.js');
};
