const LEVELS = [150, 300, 500, 750, 1000];
const VICTORY = 900;
const SUBMIT_RESULTS = true;

window.addEventListener('load', () => {
  if (!window.HTMLDialogElement) {
    document.querySelector('.large-width').innerHTML =
      '<h1>Browser issue!</h1><p>Sadly you need a browser that supports the dialog element to play Parent Quest. Google Chrome works.</p>';
  }
});

const lookupLevelName = (key) => {
  switch (key) {
    case 'common':
      return 'Streatham Common';
    case 'sewers':
      return 'Sewers below Mangetsu';
    case 'court':
      return 'Manor Court';
    case 'cemetery':
      return 'Streatham Park Cemetery';
    default:
      return 'Parent Quest';
  }
};

// Only allow the game to start if baby has a name.
const nameField = document.querySelector('#nameField');

if (nameField) {
  const nameFromLocalStorage = localStorage.getItem('babyName');
  const startLink = document.getElementById('startQuestLink');
  if (nameFromLocalStorage) {
    nameField.value = nameFromLocalStorage;
    startLink.removeAttribute('disabled');
  }

  nameField.addEventListener('input', (event) => {
    const text = event.target.value;

    if (text) {
      startLink.removeAttribute('disabled');
    } else {
      startLink.setAttribute('disabled', 'true');
    }
  });
}
// save baby name
const startLink = document.querySelector('#startQuestLink');
if (startLink)
  startLink.addEventListener('click', () => {
    const babyName = document.querySelector('#nameField').value;
    localStorage.setItem('babyName', babyName);
  });

const levelFromXp = (xp) => {
  return LEVELS.reduce((maxLevelSoFar, levelBoundary) => {
    if (xp >= levelBoundary) return maxLevelSoFar + 1;
    else return maxLevelSoFar;
  }, 1);
};

// current XP
const currentXp = () => {
  const buttons = [...document.querySelectorAll('.quest-button')];
  const xp = buttons.reduce((totalSoFar, button) => {
    if (button.classList.contains('complete'))
      return totalSoFar + Number(button.getAttribute('xp'));
    else return totalSoFar;
  }, 0);
  return xp;
};

const currentLevel = () => levelFromXp(currentXp());

// complete quest

const completeQuest = (questName) => {
  localStorage.setItem(`${questName}`, true);
};

// generate modal on click
document.querySelectorAll('.quest-button').forEach((questButton) => {
  const questKey = questButton.getAttribute('key');
  const dialog = document.getElementById(questKey);
  const closeButton = dialog.querySelector('.close-dialog');
  const completeButton = dialog.querySelector('.complete-quest');

  questButton.addEventListener('click', (event) => {
    dialog.showModal();
  });

  closeButton.addEventListener('click', () => dialog.close());

  completeButton.addEventListener('click', () => {
    const initialLevel = currentLevel();
    completeQuest(questKey);
    updateQuestButtons();
    const newLevel = currentLevel();
    if (newLevel > initialLevel) {
      document.querySelector(`.level-up[level='${newLevel}']`).showModal();
      if (SUBMIT_RESULTS) submitToServer();
    }
    dialog.close();

    if (currentXp() > VICTORY && SUBMIT_RESULTS) submitVictory();

    showVictoryIfWon();

    updateXp();
  });
});

const submitVictory = () => {
  const babyName = localStorage.getItem('babyName');

  const url = new URL('THIS WAS AN IFTTT WEBHOOK');
  url.search = new URLSearchParams({
    value1: babyName,
    value2: 'VICTORY',
  });
  fetch(url);
};

const submitToServer = () => {
  const babyName = localStorage.getItem('babyName');
  const hasReachedLevel = currentLevel();

  const url = new URL('THIS WAS AN IFTTT WEBHOOK');
  url.search = new URLSearchParams({
    value1: babyName,
    value2: hasReachedLevel,
  });
  fetch(url);
};

// update quest button UI
const updateQuestButtons = () => {
  const questButtons = document.querySelectorAll('.quest-button');
  questButtons.forEach((questButton) => {
    const key = questButton.getAttribute('key');
    if (localStorage.getItem(key)) {
      questButton.classList.add('complete');
    }
  });
};

const sectionXP = (key) => {
  const section = document.querySelector(`.game-quests[location=${key}]`);
  const questButtons = [...section.querySelectorAll('.quest-button')];
  const totalXP = questButtons.reduce((totalSoFar, button) => {
    if (button.classList.contains('complete'))
      return totalSoFar + Number(button.getAttribute('xp'));
    else return totalSoFar;
  }, 0);
  return totalXP;
};

const totalXp = () => {
  const questButtons = [...document.querySelectorAll('.quest-button')];
  const totalXp = questButtons.reduce((totalSoFar, button) => {
    if (button.classList.contains('complete'))
      return totalSoFar + Number(button.getAttribute('xp'));
    else return totalSoFar;
  }, 0);
  return totalXp;
};

const updateXp = () => {
  const sewerMeter = document.getElementById('sewer-meter');
  if (sewerMeter) sewerMeter.setAttribute('value', sectionXP('sewers'));

  const courtMeter = document.getElementById('court-meter');
  if (courtMeter) courtMeter.setAttribute('value', sectionXP('court'));

  const commonMeter = document.getElementById('common-meter');
  if (commonMeter) commonMeter.setAttribute('value', sectionXP('common'));

  const cemeteryMeter = document.getElementById('cemetery-meter');
  if (cemeteryMeter) cemeteryMeter.setAttribute('value', sectionXP('cemetery'));

  const xpMeter = document.getElementById('xp-meter');
  if (xpMeter) {
    xpMeter.setAttribute('value', totalXp());
    xpMeter.setAttribute('min', LEVELS[currentLevel() - 2]);
    xpMeter.setAttribute('max', LEVELS[currentLevel() - 1]);
    xpMeter.setAttribute('optimum', LEVELS[currentLevel() - 1]);
  }
  const xpStat = document.getElementById('xp-stat');
  if (xpStat) xpStat.textContent = totalXp();

  const levelStat = document.getElementById('level-stat');
  if (levelStat) levelStat.textContent = currentLevel();

  const nextLevelStat = document.getElementById('nextLevel-stat');
  if (nextLevelStat) nextLevelStat.textContent = LEVELS[currentLevel() - 1];

  document.querySelectorAll('.ability').forEach((ability) => {
    if (currentLevel() >= Number(ability.getAttribute('level')))
      ability.classList.remove('hidden');
  });
};

const replaceBabyNames = () => {
  const dialogTexts = document.querySelectorAll('.quest-button ~ dialog p');
  const babyName = localStorage.getItem('babyName');
  dialogTexts.forEach((textNode) => {
    textNode.textContent = textNode.textContent.replace(/baby/gi, babyName);
  });
  document.querySelectorAll('.baby-name').forEach((element) => {
    element.textContent = babyName;
  });
};

const displayOnlySection = (key) => {
  document.querySelectorAll('.location-title').forEach((title) => {
    title.textContent = lookupLevelName(key);
  });
  const dialog = document.getElementById('map-dialog');
  document.querySelector('.background').setAttribute('location', key);
  document.querySelectorAll('.game-quests').forEach((section) => {
    if (section.getAttribute('location') === key)
      section.classList.remove('hidden');
    else section.classList.add('hidden');
    dialog.close();
  });
};

const addMapThumbnailHandlers = () => {
  const showSewerButton = document.getElementById('show-sewer');
  if (showSewerButton) {
    showSewerButton.addEventListener('click', () =>
      displayOnlySection('sewers')
    );
  }

  const showCourtButton = document.getElementById('show-court');
  if (showCourtButton) {
    showCourtButton.addEventListener('click', () =>
      displayOnlySection('court')
    );
  }

  const showCommonButton = document.getElementById('show-common');
  if (showCommonButton) {
    showCommonButton.addEventListener('click', () =>
      displayOnlySection('common')
    );
  }

  const showCemeteryButton = document.getElementById('show-cemetery');
  if (showCemeteryButton) {
    showCemeteryButton.addEventListener('click', () =>
      displayOnlySection('cemetery')
    );
  }
};

const showVictoryIfWon = () => {
  if (currentXp() > VICTORY) {
    document.querySelector('#victory-dialog').showModal();
  }
};

// add handler for map button
// TODO this should probably be a nav?

const mapButton = document.getElementById('show-map');
if (mapButton)
  mapButton.addEventListener('click', () => {
    document.getElementById('map-dialog').showModal();
  });

const characterButton = document.getElementById('show-character');
if (characterButton)
  characterButton.addEventListener('click', () => {
    document.getElementById('character-dialog').showModal();
  });

// add handlers for buttons to close dialogs

document.querySelectorAll('.close-dialog').forEach((closeButton) => {
  closeButton.addEventListener('click', () =>
    closeButton.closest('dialog').close()
  );
});

updateQuestButtons();
updateXp();
showVictoryIfWon();
replaceBabyNames();
addMapThumbnailHandlers();
